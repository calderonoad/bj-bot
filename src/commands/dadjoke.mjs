import { SlashCommandBuilder } from 'discord.js';
import fetch from 'node-fetch';

const API_URL = 'https://api.api-ninjas.com/v1/dadjokes?limit=1';
const API_KEY = process.env.API_NINJA_KEY;

export const data = new SlashCommandBuilder()
  .setName('dadjoke')
  .setDescription('Get a random dad joke');

export async function execute(interaction) {
  await interaction.deferReply();

  const response = await fetch(API_URL, {
    method: 'GET',
    headers: {
      'X-Api-Key': API_KEY
    }
  });

  if (response.ok) {
    const jokes = await response.json();
    const joke = jokes[0]?.joke || 'No joke found!';
    await interaction.editReply(joke);
  } else {
    console.error('Error fetching joke:', response.status);
    await interaction.editReply('Sorry, I couldn\'t fetch a joke right now.');
  }
}
