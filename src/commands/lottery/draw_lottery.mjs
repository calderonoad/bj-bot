import {SlashCommandBuilder} from 'discord.js';
import {rollLottery} from '../../utils/lottery.mjs';

export const data = new SlashCommandBuilder()
  .setName('draw_lottery')
  .setDescription('Draw the lottery!')
  .addIntegerOption(option => option
    .setName('count')
    .setDescription('How many entries will be drawn')
  );

export async function execute(interaction) {
  await rollLottery(interaction, null, interaction.options.getInteger('count'));
}
