import {bot} from '../services/bot.mjs';
import {hasConsentedToAll, hasConsentedToOthers} from '../utils/consent.mjs';
const botnoiseId = process.env.BOTNOISE_CHANNEL_ID;

const shameRoleId = process.env.SHAME_ROLE_ID;
const punishMeRoleId = process.env.PUNISH_ME_NOW_ROLE_ID;

export default (() => {
  return {
    name: 'guildMemberUpdate',
    async execute(oldMember, newMember) {
      // Check if the user's roles were updated
      if (oldMember.roles.cache.size !== newMember.roles.cache.size) {
        // Check if user had the shame role
        const hasShameRole = oldMember.roles.cache.has(shameRoleId);
        // Check if the user no longer has the shame role
        const removeShameRole = hasShameRole && !newMember.roles.cache.has(shameRoleId);

        if (removeShameRole) {
          const shame = await bot.shame.forUser(newMember.id);
          if (shame.isShamed()) {
            await shame.clear("role removed");
          }
        }

        // Check if "punish me now" was turned on
        const hadPunishMeRole = oldMember.roles.cache.has(punishMeRoleId);
        const hasPunishMeRoleNow = newMember.roles.cache.has(punishMeRoleId);

        // if user now has the "punish me" role on, but didn't have it before
        if (!hadPunishMeRole && hasPunishMeRoleNow) {
          const botNoise = await newMember.guild.channels.fetch(botnoiseId);
          let punishMessage = `<@${newMember.id}> has turned on the "punish me now" role `;

          if (await hasConsentedToAll(newMember.id)) {
            punishMessage += 'and has consented to all! :partying_face:';
          } else if (await hasConsentedToOthers(newMember.id)) {
            punishMessage += 'but has only consented to limited people.';
          } else {
            punishMessage += 'but has not consented to anyone.\n\n';
            punishMessage += 'If you would like anyone to actually assign you anything you need ' +
              'to consent to people. You can consent to all, or individuals.' +
              '\nSee https://discord.com/channels/928037167677181952/928298290783600690/997994922378534984' +
              '\n\nUse `/consent` to give consent to people to mess with you.';
          }
          botNoise.send(punishMessage);
        }
      }
    },
  };
})();
