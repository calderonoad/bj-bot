// src/services/statistics/base.mjs
import {brain} from '../../utils/brain.mjs';

export class TrackingBase {
  #userId;
  constructor(userId) {
    this.#userId = userId;
  }

  get userId() {
    return this.#userId;
  }

  async save() {
    return brain.write();
  }
}
