import { SlashCommandBuilder } from 'discord.js';
import fetch from 'node-fetch'; // You might need to install and import node-fetch

export const data = new SlashCommandBuilder()
  .setName('handwritten_lines_assignment')
  .setDescription('Get a random quote to hand write for the server')
  .addIntegerOption(option =>
    option.setName('min_random_lines')
      .setDescription('Minimum number of times the quote should be handwritten')
      .setMinValue(1)
  )
  .addIntegerOption(option =>
    option.setName('max_random_lines')
      .setDescription('Maximum number of times the quote should be handwritten')
      .setMinValue(10)
  );

export async function execute(interaction) {
  const minLines = interaction.options.getInteger('min_random_lines') || 10; // Default to 10 if not provided
  const maxLines = interaction.options.getInteger('max_random_lines') || 50; // Default to 50 if not provided

  // Ensure minLines is not greater than maxLines
  if (minLines > maxLines) {
    await interaction.reply("Minimum random lines can't be greater than the maximum random lines.");
    return;
  }

  try {
    const response = await fetch('https://zenquotes.io/api/random');
    const data = await response.json();
    const quote = data[0]?.q;
    const author = data[0]?.a;

    // Generate a random number between minLines and maxLines for repetition
    const repetitions = Math.floor(Math.random() * (maxLines - minLines + 1)) + minLines;

    // Send the quote and the repetition instruction to the user
    await interaction.reply(`Here's your assignment:\n> "${quote}" — ${author}\n` +
      `Write it ${repetitions} times and post it to <#928399184074244166>.\nGet to it!`);
  } catch (error) {
    console.error(error);
    await interaction.reply('Sorry, something went wrong while fetching the quote.');
  }
}
