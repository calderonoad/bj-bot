import { SlashCommandBuilder } from 'discord.js';
import {getAndSortGivenConsent} from '../../utils/consent.mjs';


export const data = new SlashCommandBuilder()
  .setName('consent_given')
  .setDescription('See who all you have given consent to!')

export async function execute(interaction) {
  await interaction.reply('Give me a moment to organize the names...');

  const result = await getAndSortGivenConsent(interaction.user, interaction.guild);

  if (result.hasGivenConsent) {
    await interaction.editReply(`You've given consent to:\n${result.list}`);
  } else {
    await interaction.editReply('You haven\'t given consent to anyone yet.');
  }
}
