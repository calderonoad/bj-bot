import {PermissionFlagsBits, SlashCommandBuilder} from 'discord.js';
import {bot} from "../../services/bot.mjs";

export const data = new SlashCommandBuilder()
  .setName('shame_punish_admin_clear')
  .setDescription('Clears a user\'s shame counts')
  .addUserOption(option =>
    option.setName('user')
      .setDescription('The user to clear the shame counts for')
      .setRequired(true))
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  const user = interaction.options.getUser('user');
  const shame = await bot.shame.forUser(user.id);

  if (!shame.isShamed()) {
    await interaction.reply({
      content: `The user ${user.username} is not shamed.`,
      ephemeral: true
    });
  } else {
    shame.resetCount();
    await shame.save();
    await interaction.reply({
      content: `The user ${user.username}'s shame counts have been cleared. However, their shame remains intact.`,
      ephemeral: true
    });
  }
}
