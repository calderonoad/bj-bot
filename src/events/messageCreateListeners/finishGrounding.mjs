import {bot} from '../../services/bot.mjs';


/**
 * This tracks the user's completion of their grounding.
 * It listens for the grounding bot messages, and checks if the user has
 * completed their minimum required completion (percentage),
 * and minimum required minutes.
 *
 * If the user has not completed their grounding,
 * it displays the amount of minutes they have completed
 * and the amount they still have to go.
 *
 * If the user has completed their grounding,
 * it removes the grounding role and sends a message
 * to the user saying their grounding is over.
 */
export const finishGrounding = {
  name: 'finishGrounding',
  async execute(message) {
    // only listen to grounding bot messages
    if (message.author.id !== '1029920668428554312') return;

    // grounding bot has to mention someone. If no one's mentioned, go no further
    let mentions = message.mentions;
    if (!(mentions && mentions.users && mentions.users.size > 0)) return;

    let user = mentions.users.first(); // User object

    let content = message.content;
    let minutes_completed = content.match(/has completed a \d+ minute grounding/gmi);
    minutes_completed = +minutes_completed[0].match(/\d+/)[0];

    let correct_clicks_match = content.match(/Correct clicks:\s*(\d+)/i);
    let correct_clicks = correct_clicks_match ? parseInt(correct_clicks_match[1], 10) : 0;

    let incorrect_clicks_match = content.match(/Incorrect clicks:\s*(\d+)/i);
    let incorrect_clicks = incorrect_clicks_match ? parseInt(incorrect_clicks_match[1], 10) : 0;

    let missed_clicks_match = content.match(/Missed clicks:\s*(\d+)/i);
    let missed_clicks = missed_clicks_match ? parseInt(missed_clicks_match[1], 10) : 0;

    // Update the tracking data
    const tracking = await bot.stats.groundingForUser(user.id);
    await tracking.update({
      minutesCompleted: minutes_completed,
      correctClicks: correct_clicks,
      incorrectClicks: incorrect_clicks,
      missedClicks: missed_clicks
    });

    // Does the user have grounding settings? No grounding settings, no continue
    const grounding = await bot.grounding.forUser(user.id);
    if (!grounding.isGrounded()) return;

    // user has grounding settings
    // Let's do this!
    let completion = content.match(/Completion: \d+\.?\d*%/gmi);
    completion = +completion[0].match(/\d+\.?\d*/)[0];
    let fail = false;
    let reply = '';

    if (!grounding.metMinimumPercentage(completion)) {
      reply += 'You didn\'t complete your minimum ';
      reply += `required ${grounding.minimumPercentage}% completion. `;
      fail = true;
    }

    if (!grounding.metMinimumMinutes(minutes_completed)) {
      reply += 'You didn\'t complete the required minimum ';
      reply += `of ${grounding.minimumMinutes} minutes. `;
      fail = true;
    }

    let mistakes = incorrect_clicks + missed_clicks;
    let mistake_penalty = mistakes * grounding.mistakePenalty;
    if (grounding.hasMistakePenalty() && mistakes > 0) {
      await grounding.handleMistakes(mistakes);
      reply += `\nYou made ${mistakes} mistake${mistakes === 1 ? '' : 's'}, `;
      reply += `so your sentence is increased by ${mistake_penalty} `;
      reply += `minute${mistake_penalty === 1 ? '' : 's'}.\n`;
    }

    if (fail) {
      if (grounding.isSecret()) {
        // reset reply for secret groundings
        reply = 'You didn\'t hit one or more of your minimums.\n Too bad!! (So sad)\n';
      }
      reply = 'Oh no!! ' + reply + 'This grounding doesn\'t count =(';
      message.reply(reply);
    } else {
      // You didn't fail! Good for you.
      await grounding.addMinutesCompleted(minutes_completed);
      if (grounding.isGroundingComplete()) {
        // you're free!
        await grounding.end();
        message.reply('Your grounding is finally over.');
      } else {
        // not done yet
        let minutesCompleted = grounding.minutesDone;
        let minutesTodo = grounding.minutesToDo;
        let reply = `You have completed ${minutesCompleted} ${minutesCompleted === 1 ? 'minute' : 'minutes'}. `;
        let togo = minutesTodo - minutesCompleted;

        // get this reply, again
        if (grounding.hasMistakePenalty() && mistakes > 0) {
          reply += `\nYou made ${mistakes} mistake${mistakes === 1 ? '' : 's'}, `;
          reply += `so your sentence is increased by ${mistake_penalty} `;
          reply += `minute${mistake_penalty === 1 ? '' : 's'}.\n`;
        }

        if (grounding.isSecret()) {
          reply += '**Good luck and get to it!** ';
        } else {
          reply += `**Only ${togo} ${togo === 1 ? 'minute' : 'minutes'} of grounding left to go!**\n` +
            'Use `/grounding_check` to check the status of your grounding.';
        }
        message.reply(reply);
      }
    }
  }
}
