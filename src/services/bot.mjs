import {Client, Guild, TextChannel} from 'discord.js';

export class Bot {
    /** @var {Client<true>} */
    client;

    /** @var {Guild} */
    guild;

    /** @var {TextChannel} */
    logsChannel;

    /** @var {GroundingRepository} */
    grounding;

    /** @var {LinesRepository} */
    lines;

    /** @var {ShameRepository} */
    shame;

    /** @var {StatisticsRepository} */
    stats;
}

/** Container for accessing global services. */
export const bot = new Bot();
