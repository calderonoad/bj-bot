import { SlashCommandBuilder } from 'discord.js';
import { isSafewordActive } from '../../utils/consent.mjs';

export const data = new SlashCommandBuilder()
  .setName('safeword_status')
  .setDescription('Check your current safeword status');

export async function execute(interaction) {
  if (await isSafewordActive(interaction.user)) {
    await interaction.reply('Your safeword is currently active. ' +
      'No one can mess with you right now.');
  } else {
    await interaction.reply('Your safeword is currently not active. ' +
      'Anyone you\'ve authorized can mess with you.');
  }
}
