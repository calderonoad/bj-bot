import { SlashCommandBuilder } from 'discord.js';
import { giveConsent, giveConsentToAll } from '../../utils/consent.mjs';
import {getGuildMemberByUserID} from 'bot-commons-utils/src/utils/users.mjs';

export const data = new SlashCommandBuilder()
  .setName('consent')
  .setDescription('Give consent to a user or to all users')
  .addUserOption(option =>
    option.setName('user')
      .setDescription('The user you want to give consent to')
      .setRequired(false)
  );

export async function execute(interaction) {
  const user = interaction.options.getUser('user');

  if (user) {
    const consentStatus = await giveConsent(interaction.user, user);

    if (consentStatus) {
      await interaction.reply(`You've given consent to: ${user}`);
    } else {
      const member = await getGuildMemberByUserID(interaction.guild, user.id);
      await interaction.reply({
        content: `You have already given consent to: ${member.displayName}`,
        ephemeral: true
      });
    }
  } else {
    const consentToAllStatus = await giveConsentToAll(interaction.user);

    if (consentToAllStatus) {
      await interaction.reply('Everyone has been given consent to mess with you using my commands.');
    } else {
      await interaction.reply({
        content: 'I love the enthusiasm but you had already given consent to everyone.',
        ephemeral: true
      });
    }
  }
}
