import {
  brain,
  ensureBrainKeyExists,
  ensureBrainKeyExistsPromise,
  brainKeyOrFalse
} from 'bot-commons-utils/src/utils/brain.mjs';
import { initBadJanetBrain } from './badJanetBrain.mjs';

// Initialize the database
async function initBrain() {
  await brain.read(); // Load the database from file

  // deal with the brainless condition
  if (brain.data == null) {
    brain.data = {};
  }

  // ensure restricted word list exists
  await ensureBrainKeyExistsPromise('restricted_words');

  await completeAnyMigrations();

  // Write any changes to the database back to the file
  await brain.write();

  return brain.data;
}

async function completeAnyMigrations() {
  // put one-time migrations in here

  // fill from bad janet
  // const badJanetBrain = await initBadJanetBrain();
  // if (!brain.data.consent) {
  //   brain.data.consent = badJanetBrain.data.consent;
  // }

  // remove this key from the brain permanently
  // after this runs on live once, it can be removed
  // if (brain.data.shame_counts) {
  //   delete brain.data.shame_counts;
  // }

}

export {
  initBrain,
  brain,
  ensureBrainKeyExists,
  ensureBrainKeyExistsPromise,
  brainKeyOrFalse
};
