import { config } from 'dotenv';
import { Client, GatewayIntentBits, Partials } from 'discord.js';
import path from 'node:path';

import {bot} from './services/bot.mjs';
import {ShameRepository} from "./services/shame.mjs";
import {StatisticsRepository} from './services/statistics.mjs';
import {initBrain, brain, ensureBrainKeyExistsPromise} from './utils/brain.mjs';
import {ensureEnvironmentVariables} from 'bot-commons-utils/src/utils/env.mjs';
import {scheduleAllGagCron} from './utils/gag.mjs';
import {getRandomWeather, scheduleWeatherChange} from './utils/backfire.mjs';
import {GroundingRepository} from './services/grounding.mjs';
import {LinesRepository} from './services/lines.mjs';

import {
  registerCommands,
  registerEvents,
  registerScripts,
  registerCronJobs,
  loginBot,
  setupGracefulShutdown
} from 'bot-commons-utils/src/utils/botSetup.mjs';

await initBrain();
await ensureEnvironmentVariables(); // Ensure all environment variables are set correctly
config(); //for dotenv

// Create a new client instance
bot.client = new Client({
  partials: [Partials.Message, Partials.Channel, Partials.Reaction],
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildMembers,
    GatewayIntentBits.GuildMessageReactions,
    GatewayIntentBits.MessageContent,
    GatewayIntentBits.DirectMessages,
    GatewayIntentBits.GuildVoiceStates,
  ],
});

// Define the bot's root directory
const botRoot = path.resolve('.');
// Register all commands in the folder
await registerCommands(bot, botRoot);
// Register events
await registerEvents(bot, botRoot);
// Register scripts in the folder
await registerScripts(bot, botRoot);
// Register all cron jobs in the folder
await registerCronJobs(bot, botRoot);

// Initialize the weather when the bot starts up
await ensureBrainKeyExistsPromise('weather');
brain.data.weather = getRandomWeather();
brain.data.weather.report = null;
await brain.write();
await scheduleWeatherChange();

// Log bot into Discord, set Guild in Bot service
await loginBot(bot);
bot.logsChannel = await bot.client.channels.fetch(process.env.LOGS_ROOM);

bot.grounding = new GroundingRepository();
bot.lines = new LinesRepository();
bot.shame = new ShameRepository();
bot.stats = new StatisticsRepository();

// Register the ungag cron jobs
await scheduleAllGagCron(bot.client);

// Stop the bot when the process is closed (via Ctrl-C).
setupGracefulShutdown(bot);
