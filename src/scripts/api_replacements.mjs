
export const dog = {
  regex: /^\?dog/i,
  tag_bot: false,
  execute: async function(message) {
    message.reply('Please use /random_pic dog');
  },
};


export const cat = {
  regex: /^\?cat/i,
  tag_bot: false,
  execute: async function(message) {
    message.reply('Please use /random_pic cat');
  },
};


export const dadjoke = {
  regex: /^\?dadjoke/i,
  tag_bot: false,
  execute: async function(message) {
    message.reply('Please use /dadjoke');
  },
};

export const oddjob = {
  regex: /^\?oddjob/i,
  tag_bot: false,
  execute: async function(message) {
    message.reply('Please use /oddjob');
  },
};
