import { SlashCommandBuilder } from 'discord.js';
import fetch from 'node-fetch';

export const data = new SlashCommandBuilder()
  .setName('random_pic')
  .setDescription('Get a random picture of something!')
  .addSubcommand(subcommand =>
    subcommand
      .setName('dog')
      .setDescription('Get a random dog picture'))
  .addSubcommand(subcommand =>
    subcommand
      .setName('cat')
      .setDescription('Get a random cat picture'));

export async function execute(interaction) {
  const subcommandName = interaction.options.getSubcommand();
  await interaction.deferReply();
  await interaction.editReply('Fetching picture!');

  let apiUrl, parseImageUrl;

  if (subcommandName === 'dog') {
    apiUrl = 'https://dog.ceo/api/breeds/image/random';
    parseImageUrl = data => data.status === 'success' ? data.message : null;
  } else if (subcommandName === 'cat') {
    apiUrl = 'https://api.thecatapi.com/v1/images/search';
    parseImageUrl = data => data[0]?.url;
  }

  try {
    const imageUrl = await fetchRandomImage(apiUrl, parseImageUrl);

    if (imageUrl) {
      await interaction.editReply({ content: `Here's a random ${subcommandName} for you:`, embeds: [{ image: { url: imageUrl } }] });
    } else {
      await interaction.editReply(`Sorry, something went wrong while fetching the ${subcommandName} image.`);
    }
  } catch (error) {
    console.error(error);
    await interaction.editReply('Sorry, there was an error fetching the image.');
  }
}

async function fetchRandomImage(apiUrl, parseImageUrl) {
  const response = await fetch(apiUrl);
  const data = await response.json();
  return parseImageUrl(data);
}
