import { encode } from 'gpt-3-encoder';

function formatMessageWithEmbeds(message, forMeeting = false, includeDates = true) {
  if (forMeeting) return formatMeetingMessage(message);

  let formattedMessage = includeDates
      ? `[${message.createdAt}] ${message.author.tag}: ${message.content}\n`
      : `${message.author.tag}: ${message.content}\n`;

  if (message.embeds.length > 0) {
    message.embeds.forEach(embed => {
      if (embed.title) formattedMessage += `Embed Title: ${embed.title}\n`;
      if (embed.description) formattedMessage += `Embed Description: ${embed.description}\n`;
      if (embed.fields && embed.fields.length > 0) {
        formattedMessage += 'Embed Fields:\n';
        embed.fields.forEach(field => {
          formattedMessage += `  - ${field.name}: ${field.value}\n`;
        });
      }
    });
  }

  return formattedMessage;
}

export function formatMeetingMessage(message) {
  let formattedMessage = '';

  if (message.author.id === '1001955060210749492') {
    formattedMessage = `${message.content}\n`;
  } else if (message.author.username.includes('[Scriptly]')) {
    // Remove [Scriptly] from the message
    let cleanedTag = message.author.tag.replace('[Scriptly] ', '');
    formattedMessage = `${cleanedTag}: ${message.content}\n`;
  } else {
    formattedMessage = `${message.author.tag}: ${message.content}\n`;
  }

  if (message.embeds.length > 0) {
    message.embeds.forEach(embed => {
      if (embed.title) formattedMessage += `Embed Title: ${embed.title}\n`;
      if (embed.description) formattedMessage += `Embed Description: ${embed.description}\n`;
      if (embed.fields && embed.fields.length > 0) {
        formattedMessage += 'Embed Fields:\n';
        embed.fields.forEach(field => {
          formattedMessage += `  - ${field.name}: ${field.value}\n`;
        });
      }
    });
  }
  return formattedMessage;
}

// Known bug: if a Discord message can be larger than the GPT token limit, this might not work. But that shouldn't happen
export function splitMessagesByTokenLimit(messages, tokenLimit, forMeeting = false, includeDates = true) {
  const result = [];
  let currentChunk = '';
  let currentChunkTokens = 0;

  messages.forEach((msg) => {
    const formattedMessage = formatMessageWithEmbeds(msg, forMeeting, includeDates);
    const msgTokens = encode(formattedMessage).length;

    if (currentChunkTokens + msgTokens <= tokenLimit) {
      currentChunk += formattedMessage + '\n';
      currentChunkTokens += msgTokens;
    } else {
      result.push(currentChunk.trim());
      currentChunk = formattedMessage + '\n';
      currentChunkTokens = msgTokens;
    }
  });

  if (currentChunk) result.push(currentChunk.trim());

  return result;
}

export function splitMessagesBySize(messages, sizeLimit, forMeeting = false, includeDates = true) {
  const result = [];
  let currentChunk = '';
  let currentChunkSize = 0;

  messages.forEach((msg) => {
    const formattedMessage = formatMessageWithEmbeds(msg, forMeeting, includeDates);
    const msgSize = Buffer.byteLength(formattedMessage, 'utf8') + 1; // +1 for the newline character

    if (currentChunkSize + msgSize <= sizeLimit) {
      currentChunk += formattedMessage + '\n';
      currentChunkSize += msgSize;
    } else {
      result.push(currentChunk.trim());
      currentChunk = formattedMessage + '\n';
      currentChunkSize = msgSize;
    }
  });

  if (currentChunk) result.push(currentChunk.trim());

  return result;
}
