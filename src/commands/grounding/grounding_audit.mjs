import { SlashCommandBuilder, PermissionFlagsBits } from 'discord.js';
import { bot } from '../../services/bot.mjs';
import { MessageStacker } from '../../utils/classes/MessageStacker.mjs';

export const data = new SlashCommandBuilder()
  .setName('grounding_audit')
  .setDescription('Audits what\'s in the brain for groundings')
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  await interaction.deferReply({ ephemeral: true });

  const stacker = new MessageStacker(interaction, '');

  for await (const grounding of bot.grounding.allUsers()) {
    let reply = '';
    try {
      reply += grounding.statusMessage;
    } catch (error) {
      reply += `**<@${grounding.getUserId()}>** (Unknown user)\n`;
    }
    await stacker.appendOrSend(reply);
  }

  await stacker.finalize('', 'No one is grounded.');
}
