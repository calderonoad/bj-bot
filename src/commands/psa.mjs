// src/commands/psa.mjs
import { SlashCommandBuilder } from 'discord.js';
import { ActionRowBuilder, StringSelectMenuBuilder } from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('psa')
  .setDescription('Displays a public service announcement.')
  .addStringOption(option =>
    option.setName('visibility')
      .setDescription('Choose how to display the message')
      .setRequired(true)
      .addChoices(
        { name: 'Show just to me', value: 'ephemeral' },
        { name: 'Post publicly', value: 'public' }
      )
  );

export async function execute(interaction) {
  const visibility = interaction.options.getString('visibility');

  // PSA Options as select menu
  const row = new ActionRowBuilder()
    .addComponents(
      new StringSelectMenuBuilder()
        .setCustomId(`select-psa_${visibility}`) // Pass visibility in custom ID
        .setPlaceholder('Choose a PSA')
        .addOptions([
          {
            label: 'Diet/Exercise and D/s',
            description: 'Cautions on why not to mix Diet/Exercise and D/s.',
            value: 'diet_and_ds',
          },
          {
            label: 'Basic Bondage Safety',
            description: 'Plan for emergencies',
            value: 'bondage',
          },
          {
            label: 'Therapy PSA',
            description: 'We are a Therapy-Positive Community.',
            value: 'therapy',
          },
        ]),
    );

  await interaction.reply({ content: 'Please select a PSA:', components: [row], ephemeral: true });
}

// Handler for select menus
export async function handleSelectMenu(interaction) {
  const parts = interaction.customId.split('_'); // Split to extract visibility
  const visibility = parts[1];
  const ephemeral = visibility === 'ephemeral';

  let responseMessage;

  switch (interaction.values[0]) {
    case 'diet_and_ds':
      responseMessage = '## PSA on BDSM and Diet/Exercise Control. \n\n' +
        'This approach can be deeply engaging, but it carries significant risks that need careful management. ' +
        'It is strongly recommended that you do not engage in this type of play, ' +
        'or if you do, proceed with extreme caution and consider the safety aspects. \n\n' +
        '' +
        '**Risk of Eating Disorders:** While it seems easy to put a dom in charge so that you ' +
        'must eat right and exercise, this is generally an unhealthy approach, *especially* if coupled with ' +
        'punishments. Control over eating and exercise can create, trigger, or exacerbate eating disorders. ' +
        'It is also very, very easy to slip into an unhealthy spiral when punishments ' +
        'are linked to dietary or exercise behaviors. \n' +
        '**The stakes are high, and the consequences can be severe.**\n\n' +
        '' +
        '**Consider Long-Term:** Consider what happens when the dynamic ends. Will the sub still continue healthy ' +
        'practices? Make a plan for how to ensure that this is a long-term lifestyle change that does not ' +
        'rely solely on the dynamic and the presence of a dominant.\n\n' +
        '' +
        '**Therapy:** Often, underlying issues contribute to weight management challenges. ' +
        'A therapist can provide crucial support in addressing these issues. \n\n' +
        '' +
        '**Other Professional Oversight:** Any alterations to diet or exercise regimes should be guided by qualified ' +
        'professionals—doctors, dietitians, trainers, etc. \n\n' +
        '' +
        '**Weight Loss Websites Suck:** Many weight loss websites offer questionable advice. Always consult with a ' +
        'professional instead of relying on online sources; someone\'s health is at stake. \n\n' +
        '' +
        '**Considerations:** ' +
        '\n* Accountability can be helpful, ' +
        'but it should also be there to prevent over-exercising or under-eating, too. ' +
        '\n* Do not issue punishments for slip-ups, but provide encouragement for healthy successes. ' +
        '\n* Focus on overall health rather than the number on the scale. ' +
        '\n* If at all possible, get professionals involved in creating a long-term plan and ' +
        'follow their advice.\n\n' +
        '' +
        'Of course, we\'re all adults here, so you do what you\'re going to do.';
      break;
    case 'bondage':
      responseMessage = '## PSA on Safety in Bondage Practices\n\n' +
        'When engaging in bondage, safety should always be your top priority. ' +
        'It is critically important never to leave a person in bondage unattended. ' +
        'The risks of leaving someone alone include potential circulation issues, injury from ' +
        'constraints, or other emergencies where immediate assistance is required.\n\n' +
        '' +
        '**Never Alone:** Always be or have a responsible person present who can quickly and safely ' +
        'release the individual from bondage. ' +
        'Emergencies can occur unexpectedly, and the ability to react swiftly could prevent serious harm.\n\n' +
        '' +
        '**Self-Bondage Caution:** If you are practicing self-bondage, it is crucial to set up a ' +
        'fail-safe release mechanism that you can operate under any circumstances. ' +
        'Remember, in self-bondage, you must act as both the participant and the safety monitor. Always ensure that:\n' +
        '* You have quick-release mechanisms within easy reach.\n' +
        '* You inform someone trustworthy about your activities and establish check-ins.\n' +
        '* You have a backup plan, such as a nearby phone that you can use to call for help if needed.\n\n' +
        '' +
        '**General Safety Tips:**\n' +
        '* Always use tools and materials that are designed for bondage and have safety features.\n' +
        '* Regularly practice how to quickly release yourself or another from bondage, to ' +
        'ensure you are prepared in an emergency.\n' +
        '* Keep scissors or other cutting tools that can safely cut through your binding ' +
        'material close at hand in case quick release mechanisms fail.\n\n' +
        '';

      break;
    case 'therapy':
      responseMessage = '## Therapy-Positive Community\n\n' +
        'As a community that embraces diverse lifestyles, ' +
        'we recognize the importance of mental health support for all our members.\n\n' +
        '' +
        '**Destigmatizing Therapy:** We are committed to breaking down the stigma around seeking therapy. ' +
        'It’s essential to treat mental health with the same seriousness as physical health, ' +
        'acknowledging that everyone, regardless of their lifestyle choices, can benefit from professional support.\n\n' +
        '' +
        '**Finding the Right Therapist:** While finding a therapist who is knowledgeable about ' +
        'BDSM can be challenging, finding a therapist who understands the complexities of ' +
        'BDSM can offer more nuanced and relevant support. \n\n' +
        '' +
        '**Encouraging Its Use:** We encourage everyone to consider therapy, not just in times of ' +
        'crisis, but as a regular part of maintaining mental wellness. This community supports your ' +
        'journey toward better mental health.\n\n' +
        '' +
        '**Resources and Support:** If you need help finding a therapist or have questions about ' +
        'how therapy can be tailored to your needs, please reach out. Our aim is to support each ' +
        'other in all aspects of well-being.\n' +
        '* [Kink Aware Mental Health Professionals](https://www.kapprofessionals.org/kap_directory/kap_category/counselors-and-therapists/)\n' +
        '';


      break;
    case 'psa3':
      responseMessage = 'Details of Example PSA 3.';
      break;
    default:
      responseMessage = 'No PSA selected.';
      break;
  }

  console.log(ephemeral);
  if (ephemeral) {
    // Update the ephemeral interaction directly
    await interaction.update({ content: responseMessage, components: [] });
  } else {
    // For public visibility, end the current ephemeral interaction and send a new public message
    await interaction.update({ content: 'Please check the channel for the PSA.', components: [], ephemeral: true });
    await interaction.followUp({ content: responseMessage, ephemeral: false });
  }
}
